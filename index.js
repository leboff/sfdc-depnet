var yargs = require('yargs');
var fs = require('fs');
var sfdc = require('./lib/sfdc');
var process = require('./lib/process')

function relatedCode(yargs){
  let config = yargs.argv;
  let conn = new sfdc(config);
  
  return conn.getRelatedCode();

}

yargs
.config('settings', function(configPath){
  return JSON.parse(fs.readFileSync(configPath, 'utf-8'))
})
.option('f', {
  alias: 'file',
  default: 'output.json'
})
.option('o', {
  alias: 'output'
})
.command(
  'related',
  'get all related code',
  relatedCode
)
.command(
  'edges',
  'process output.json file into edges',
  process.edges
)
.command(
  'nodes',
  'process output.json file into nodes',
  process.nodes
)
.help()
.argv



