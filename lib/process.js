const fs = require('fs-extra');
const split2 = require('split2');
const miss = require('mississippi');
const es = require('event-stream')
const stringify = require('csv-stringify');

var nodeStringify = stringify({ header: true, columns: {
    id: 'id',
    name: 'name',
    type: 'type'
}});

var edgeStringify = stringify({ header: true, columns: {
    source: 'source',
    target: 'target'
}});

var processNodes = function(yargs){
    var ids = [];
    miss.pipe(
        fs.createReadStream(yargs.argv.file),
        split2(),
        es.parse(),
        miss.through.obj(function(obj, enc, cb){
            var stream = this;
            var objNode = {
                id: obj.Id,
                name: obj.Name,
                type: obj.attributes.type
            }
    
            if(!ids.includes(objNode.id)){
                ids.push(objNode.id);
                stream.push(objNode);
            }
    
            if(obj.related){
                obj.related.forEach(function(related){
                    var relatedNode = {
                        id: related.id,
                        name: related.name,
                        type: related.type
                    }
                    if(!ids.includes(relatedNode.id)){
                        ids.push(relatedNode.id);
                        stream.push(relatedNode);
                    }
                })
            }
            cb()
        }),
        nodeStringify,
        fs.createWriteStream(yargs.argv.output || 'nodes.csv'),
        function(err){
            if(err) return  console.log(err);

            console.log('Nodes written');


        }
    )
}


var processEdges = function(yargs){
    fs.createReadStream(yargs.argv.file)
    .pipe(split2())
    .pipe(es.parse())
    .pipe(miss.through.obj(function(obj, enc, cb){
        var stream = this;

        if(obj.related){
            obj.related.forEach(function(related){
                if(related.type != 'ExternalString'){
                    if(related.direction == 'References'){
                        stream.push({
                            source: obj.Id,
                            target: related.id
                        })
                    }
                    else{
                        stream.push({
                            source: related.id,
                            target: obj.Id
                        })
                    }
                }
                
              
            })
        }
        cb()
    }))
    .pipe(edgeStringify)
    .pipe(fs.createWriteStream(yargs.argv.output || 'edges.csv'));
}
module.exports = {
    edges: processEdges,
    nodes: processNodes
}