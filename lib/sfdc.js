const request = require('request-promise');
const jsforce = require('jsforce');
const Promise = require('bluebird');
const fs = require('fs-extra')
const miss = require('mississippi')
const es = require('event-stream');



const typeMap = {
    'ApexClass': 'CODE',
    'ApexTrigger': 'CODE',
    'ApexPage': 'PAGES',
    'ApexComponent': 'PAGES'
}

var Salesforce = function(config){
    this.config = config;
    this.conn = new jsforce.Connection({
        loginUrl: config.serverurl || 'https://test.salesforce.com'
    })
}

Salesforce.prototype.get = function(query, extent){
    let _self = this;
    let conn = this.conn;

    let stream = miss.through.obj();

    conn.query(query)
    .on("record", function(record) {
        stream.write(record);
    })
    .on("end", function() {
        stream.end();
    })
    .on("error", function(err) {
        stream.error(error);
    })
    .run({ autoFetch : true, maxFetch : 10000 }); // synonym of Query#execute();
    
    return stream;
}

Salesforce.prototype.getAll = function(){
    return es.merge([
        this.get('SELECT Id, Name from ApexClass where NamespacePrefix = \'\'', 'CODE'),
        this.get('Select Id, Name from ApexTrigger where NamespacePrefix = \'\'', 'CODE'),
        this.get('Select Id, Name from ApexPage where NamespacePrefix = \'\'', 'PAGES'),
        this.get('Select Id, Name from ApexComponent where NamespacePrefix = \'\'', 'PAGES')
    ])
    
}


Salesforce.prototype.login = function(){
    let config = this.config;
    console.log(config);
    return this.conn.login(config.username, config.password);
}

Salesforce.prototype.getReferences = function(){
    var conn = this.conn;
    // return this.conn.tooling.query('select id, SymbolTable from ApexClass where Id = \'01p0Z000006bp9PQAQ\'');
    return miss.through.obj(function(obj, enc, cb){
        var url = conn.instanceUrl + '/_ui/common/apex/debug/ApexCSIAPI'
        var j = request.jar();
        var cookie = request.cookie('sid='+conn.accessToken);

        j.setCookie(cookie, url);
        
        var options = {
            url: url,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            form: {
                action: 'RELATED',
                extent: typeMap[obj.attributes.type],
                relatedEntityEnumOrId: obj.Id
            },
            jar: j
        }
        console.log(`Requesting ${obj.attributes.type} ${obj.Name}...`);
        request.post(options, function(error, response, body){
            if(error) return  cb(error);
            var instances = JSON.parse(body.split('\n')[1]);
            obj.related = instances.relatedInstances.relatedInstances;
            cb(null,  obj);
        })
    })
}

Salesforce.prototype.getRelatedCode = function(){
    let _self = this;
    this.login()
    .then(function(userInfo){
        console.log(_self.config);
        miss.pipe(
            _self.getAll(),
            _self.getReferences(),
            es.stringify(),
            fs.createWriteStream(_self.config.file),
            function(err){
                if(err) return console.log('err');
                console.log('Processing complete');
            }
        )
    })
    .catch(function(e){
        console.log(e);
    })
}


module.exports = Salesforce;